//
//  WorkExperience.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-28.
//

import UIKit

class WorkExperience: UIViewController {
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var jobTitle: UITextField!
    @IBOutlet weak var presentSwitch: UISwitch!
    let years = (1990...2021).map { String($0) }
    var pickerViewArray = [UITextField]()
    let months = ["Jan" , "Feb" , "Mar" , "Apr" , "May", "Jun" , "Jul" , "Aug" , "Sep" , "Oct" , "Nov" , "Dec" ]
    @IBOutlet weak var monthTo: UITextField!
    @IBOutlet weak var yearTo: UITextField!
    @IBOutlet weak var monthFrom: UITextField!
    @IBOutlet weak var yearFrom: UITextField!
    @IBOutlet weak var addButton: UIButton!
    var editData : ExperinceList?
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Work Experience"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contentView.layer.cornerRadius = 20
        addButton.layer.cornerRadius = 25
        addButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        addButton.layer.borderWidth = 3
        pickerViewArray = [yearFrom , monthFrom , yearTo , monthTo]

        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)

        }
        dismissPickerView()
       if let safeData = editData
       {
        monthTo.text = safeData.tomonth!
        monthFrom.text = safeData.frommonth!
        yearTo.text = safeData.toyear!
        yearFrom.text = safeData.fromyear!
        companyName.text = safeData.companyName!
        jobTitle.text = safeData.jobTitle!
       }
        jobTitle.delegate = self
            companyName.delegate = self
            jobTitle.tag = 1000
                companyName.tag = 1000
        
    }
    func createPickerView(textField : UITextField , tag : Int) {
           let pickerView = UIPickerView()
           pickerView.delegate = self
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    @objc func action() {
          view.endEditing(true)
    }
    @IBAction func addButtonPressed(_ sender: Any) {
        var showAlert = 0
        if jobTitle.text == "" || companyName.text == ""
        {
            showAlert = 1
        }
        for textField in pickerViewArray
        {
            if textField.text == ""
            {
                showAlert = 1
            }
            
        }
        if showAlert == 1
        {
            let alert = UIAlertController(title: "Add Language", message: "Please fill in all fields", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
           
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            print("dad")
            if editData == nil
            {
            makePostCall()
            }
            else
            {
                makePostCallUpdate()
            }

        }
    }
    func makePostCall() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["company_name":"\(companyName.text!)","frommonth":"\(monthFrom.text!)","fromyear":"\(yearFrom.text!)","job_title":"\(jobTitle.text!)","jobseekerid":"\(id)","name":"\(name)","tomonth":"\(monthTo.text!)","toyear":"\(yearTo.text!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/insert_experience")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Work Experience", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                            _ = self.navigationController?.popViewController(animated: true)

                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Work Experience", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    func makePostCallUpdate() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["company_name":"\(companyName.text!)","frommonth":"\(monthFrom.text!)","fromyear":"\(yearFrom.text!)","job_title":"\(jobTitle.text!)","jobseekerid":"\(id)","name":"\(name)","tomonth":"\(monthTo.text!)","toyear":"\(yearTo.text!)" , "experienceid":"\(editData!.experienceid!)",]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/update_experience")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Work Experience", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                            _ = self.navigationController?.popViewController(animated: true)

                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Work Experience", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
}


extension WorkExperience : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return years.count
        case 1:
            return months.count
        case 2:
            return years.count
        case 3:
            return months.count
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return years[row]
        case 1:
            return months[row]
        case 2:
            return years[row]
        case 3:
            return months[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
 
        switch pickerView.tag {
        case 0:
            pickerViewArray[0].text = years[row]
        case 1:
            pickerViewArray[1].text = months[row]
        case 2:
            pickerViewArray[2].text = years[row]
        case 3:
            pickerViewArray[3].text = months[row]
        default:
            print("err")
        }
    
}
}
