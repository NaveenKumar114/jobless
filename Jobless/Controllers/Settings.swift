//
//  Settings.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-07.
//

import UIKit

class Settings: UIViewController {
    @IBOutlet weak var updateProfileLabel: UILabel!
    
    @IBOutlet weak var homeLbel: UILabel!
    @IBOutlet weak var logout: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profileImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let nameStr = UserDefaults.standard.string(forKey: "name")!
        name.text = nameStr
        if let image = Dashboard.imageStatic
        {
            profileImage.image = image
            profileImage.contentMode = .scaleAspectFit
            profileImage.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        }
        if let data = Dashboard.profileDataStatic
        {
            email.text = data.personaldetails?.email ?? ""
            role.text = data.personaldetails?.jobTitle ?? ""
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(updateProfile))
        updateProfileLabel.isUserInteractionEnabled = true
        updateProfileLabel.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(logOutfunc))
        logout.isUserInteractionEnabled = true
        logout.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(home))
        homeLbel.isUserInteractionEnabled = true
        homeLbel.addGestureRecognizer(tapGestureRecognizer3)
    }
    @objc func home()
    {
        self.dismiss(animated: true, completion: nil)

    }
    @objc func updateProfile()
    {
        self.dismiss(animated: true, completion: nil)

        NotificationCenter.default.post(name: Notification.Name("profile"), object: nil)

    }

    @objc func logOutfunc()
    {
        let alert = UIAlertController(title: "LogOut", message: "Are you Sure", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "id")
            defaults.removeObject(forKey: "name")
            defaults.removeObject(forKey: "isLoggedIn")
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            
         
            let loginController = storyboard.instantiateViewController(identifier: "login")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }
}
