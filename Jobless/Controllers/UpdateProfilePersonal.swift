//
//  UpdateProfile.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-28.
//

import UIKit
import DatePicker

class UpdateProfilePersonal: UIViewController {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var nationalityOthers: UITextField!
    @IBOutlet weak var postCode: UITextField!
    @IBOutlet weak var cityName: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var nationality: UITextField!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var religion: UITextField!
    @IBOutlet weak var race: UITextField!
    @IBOutlet weak var maritalStatus: UITextField!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    var pickerViewArray = [UITextField]()
   // var selectedItems = [String]()
    var dataPickerView = [["Male" , "Female"] ,
                          ["Single" , "Separated" , "Divorced" , "Widowed" , "Others"] ,
                          ["Muslim" , "Chinese" , "Hindu" , "Buddhism" , "Punjabi"] ,
                          ["Islam" , "Chinese" , "Indian" , "Christian"] ,
                          ["Malaysian" , "Non Malaysian"] ,
                          ["Malaysia"] ,
                          ["Johor" , "Kedah" , "Kelantan" , "Kuala Lumpur" , "Labuan" , "Malacca" , "Negeri Sembilan" , "Pahang" , "Penang" , "Perak" , "Perlis" , "Putrajaya" , "Sabah" , "Sarawak" , "Selangor" , "Terengganu"]
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Personal Details"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contentView.layer.cornerRadius = 20
        nextButton.layer.cornerRadius = 20
        pickerViewArray = [gender , maritalStatus , race , religion , nationality , country , state]
        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)

        }
        dateButton.setTitle("1970-01-01", for: .normal)
        dismissPickerView()
        if let safeData = Dashboard.profileDataStatic
        {
            firstName.text = safeData.personaldetails?.firstName ?? ""
            lastName.text = safeData.personaldetails?.lastName ?? ""
            email.text = safeData.personaldetails?.email ?? ""
            postCode.text = safeData.personaldetails?.pincode ?? ""
            cityName.text = safeData.personaldetails?.city ?? ""
            address.text = safeData.personaldetails?.address ?? ""
            contactNumber.text = safeData.personaldetails?.mobile ?? ""
            nationality.text = safeData.personaldetails?.nationality ?? ""
            state.text = safeData.personaldetails?.state ?? ""
            country.text = safeData.personaldetails?.country ?? ""
            religion.text = safeData.personaldetails?.religion ?? ""
            race.text = safeData.personaldetails?.race ?? ""
            maritalStatus.text = safeData.personaldetails?.maritalStatus ?? ""
            gender.text = safeData.personaldetails?.gender ?? ""
            if pickerViewArray[4].text == "Malaysian"
            {
                nationalityOthers.isEnabled = false
                nationalityOthers.alpha = 0.5
            }
            else
            {
                nationalityOthers.isEnabled = true
                nationalityOthers.alpha = 1
            }
        }
        let defaults = UserDefaults.standard

        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("https://jobless.my/uploads/jobseeker/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    if data != nil
                    {
                        print("profileImg load")
                        profileImageView.image = UIImage(data: data!)
                        profileImageView.contentMode = .scaleAspectFit
                        profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
                    }
                   
                }
            }
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toImage))
            profileImageView.isUserInteractionEnabled = true
            profileImageView.addGestureRecognizer(tapGestureRecognizer)
        nationalityOthers.delegate = self
        nationalityOthers.tag = 1000
        address.delegate = self
        address.tag = 1001
        cityName.tag = 1002
        cityName.delegate = self
        postCode.delegate = self
        postCode.tag = 1003
        contactNumber.delegate = self
        contactNumber.tag = 1004
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: Notification.Name("update"), object: nil)
    }
    @objc func updateData()
    {
       
        let defaults = UserDefaults.standard
        
       
        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("https://jobless.my/uploads/jobseeker/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    if data != nil
                    {
                        print("profileImg load")
                        profileImageView.image = UIImage(data: data!)
                        profileImageView.contentMode = .scaleAspectFit
                        profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
                    }
                   
                }
            }
        }
        
    }
    @objc func toImage()
    {
        performSegue(withIdentifier: segueIdentifiers.profToImage, sender: nil)
    }
    @IBAction func dateButtonClicked(_ sender: Any) {
        let minDate = DatePickerHelper.shared.dateFrom(day: 01, month: 01, year: 1970)!
        let maxDate = DatePickerHelper.shared.dateFrom(day: 31, month: 12, year: 2005)!
        let today = Date()
        // Create picker object
        let datePicker = DatePicker()
        // Setup
        datePicker.setup(beginWith: today, min: minDate, max: maxDate) { (selected, date) in
            if selected, let selectedDate = date {
                self.dateButton.setTitle("\(selectedDate.year())-\(selectedDate.month())-\(selectedDate.day())", for: .normal)
            } else {
                print("Cancelled")
            }
        }
        // Display
        datePicker.show(in: self, on: sender as? UIView)
    }
    func createPickerView(textField : UITextField , tag : Int) {
           let pickerView = UIPickerView()
           pickerView.delegate = self
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    @objc func action() {
          view.endEditing(true)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        var showAlert = 0
        if contactNumber.text == "" || address.text == "" || cityName.text == "" || postCode.text == "" || firstName.text == "" || lastName.text == ""
        {
            showAlert = 1
        }
        for textField in pickerViewArray
        {
            if textField.text == ""
            {
                showAlert = 1
            }
        }
        if showAlert == 1
        {
            let alert = UIAlertController(title: "Update Profile", message: "Please fill in all fields", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
           
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            makePostCall()

        }
    }
    
    func makePostCall() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let decoder = JSONDecoder()
        let json: [String: Any] = ["address" : "\(address.text!)" ,
                                   "birthdate":"\(dateButton.currentTitle!)",
                                   "city":"\(cityName.text!)",
                                   "country":"\(country.text!)",
                                   "email":"\(email.text!)",
                                   "first_name":"\(firstName.text!)",
                                   "gender":"\(gender.text!)",
                                   "jobseekerid":"\(id)",
                                   "last_name":"\(lastName.text!)",
                                   "marital_other":"",
                                   "marital_status":"\(maritalStatus.text!)",
                                   "mobile":"\(contactNumber.text!)",
                                   "nationality":"\(nationality.text!)",
                                   "other_nationality":"\(nationalityOthers.text!)",
                                   "pincode":"\(postCode.text!)",
                                   "race":"\(race.text!)",
                                   "race_other":"",
                                   "religion":"\(religion.text!)",
                                   "religion_other":"",
                                   "state":"\(state.text!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/update_profiledetails")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                            self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    
}

extension UpdateProfilePersonal : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return dataPickerView[0].count
        case 1:
            return dataPickerView[1].count
        case 2:
            return dataPickerView[2].count
        case 3:
            return dataPickerView[3].count
        case 4:
            return dataPickerView[4].count
        case 5:
            return dataPickerView[5].count
        case 6:
            return dataPickerView[6].count
        case 7:
            return dataPickerView[7].count // no need for 7
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return dataPickerView[0][row]
        case 1:
            return dataPickerView[1][row]
        case 2:
            return dataPickerView[2][row]
        case 3:
            return dataPickerView[3][row]
        case 4:
            return dataPickerView[4][row]
        case 5:
            return dataPickerView[5][row]
        case 6:
            return dataPickerView[6][row]
        case 7:
            return dataPickerView[7][row] // no need for 7
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
 
        switch pickerView.tag {
        case 0:
            pickerViewArray[0].text = dataPickerView[0][row]
        case 1:
            pickerViewArray[1].text = dataPickerView[1][row]
        case 2:
            pickerViewArray[2].text = dataPickerView[2][row]
        case 3:
            pickerViewArray[3].text = dataPickerView[3][row]
        case 4:
            pickerViewArray[4].text = dataPickerView[4][row]
            if pickerViewArray[4].text == "Malaysian"
            {
                nationalityOthers.isEnabled = false
                nationalityOthers.alpha = 0.5
            }
            else
            { 
                nationalityOthers.isEnabled = true
                nationalityOthers.alpha = 1
            }
        case 5:
            pickerViewArray[5].text = dataPickerView[5][row]
        case 6:
            pickerViewArray[6].text = dataPickerView[6][row]
        case 7:
            pickerViewArray[7].text = dataPickerView[7][row] // no need for 7
        default:
            print("err")
        }
    
}
}
