//
//  UpdateProfileProfessional.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-28.
//

import UIKit

class UpdateProfileProfessional: UIViewController {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var jobTitle: UITextField!
    
    @IBOutlet weak var socialLink: UITextField!
    @IBOutlet weak var expectedSalary: UITextField!
    @IBOutlet weak var currentSalary: UITextField!
    @IBOutlet weak var totalExperience: UITextField!
    @IBOutlet weak var category: UITextField!
    @IBOutlet weak var industry: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var contetnView: UIView!
    var pickerViewArray = [UITextField]()
   // var selectedItems = [String]()
    var dataPickerView = [["Male" , "Female"] ,
                          ["Single" , "Separated" , "Divorced" , "Widowed" , "Others"] ,
                          ["Muslim" , "Chinese" , "Hindu" , "Buddhism" , "Punjabi"] ,
                          ["Islam" , "Chinese" , "Indian" , "Christian"] ,
                          ["Malaysian" , "Non Malaysian"] ,
                          ["Malaysia"] ,
                          ["Johor" , "Kedah" , "Kelantan" , "Kuala Lumpur" , "Labuan" , "Malacca" , "Negeri Sembilan" , "Pahang" , "Penang" , "Perak" , "Perlis" , "Putrajaya" , "Sabah" , "Sarawak" , "Selangor" , "Terengganu"]
    ]
    var industryListData : IndustryListJSON?
    var selectedIndustry = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Professional Details"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contetnView.layer.cornerRadius = 20
        addButton.layer.cornerRadius = 25
        addButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        addButton.layer.borderWidth = 3
        makeGetCall()
        pickerViewArray = [category , industry]
        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)

        }
        if let safeData = Dashboard.profileDataStatic
        {
        /*    @IBOutlet weak var jobTitle: UITextField!
            
            @IBOutlet weak var socialLink: UITextField!
            @IBOutlet weak var expectedSalary: UITextField!
            @IBOutlet weak var currentSalary: UITextField!
            @IBOutlet weak var totalExperience: UITextField!
            @IBOutlet weak var category: UITextField!
            @IBOutlet weak var industry: UITextField! */
            if let s = safeData.personaldetails
            {
                jobTitle.text = s.jobTitle ?? ""
                socialLink.text = s.website ?? ""
                expectedSalary.text = s.expectedMax ?? ""
                currentSalary.text = s.currentSalary ?? ""
                totalExperience.text = s.totalExperience ?? ""
                category.text = s.jobDesignation ?? ""
                industry.text = s.industry ?? ""
            }
        }
        dismissPickerView()
        jobTitle.delegate = self
        totalExperience.delegate = self
        currentSalary.delegate = self
        expectedSalary.delegate = self
        socialLink.delegate = self
        
        jobTitle.tag = 1000
        totalExperience.tag = 10001
        currentSalary.tag = 1002
        expectedSalary.tag = 1004
        socialLink.tag = 1005

    }
    func createPickerView(textField : UITextField , tag : Int) {
           let pickerView = UIPickerView()
           pickerView.delegate = self
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    @objc func action() {
          view.endEditing(true)
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
    
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://jobless.my/andorid_api/Jobseeker_helper/getInductryList")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let industryList = try? decoder.decode(IndustryListJSON.self, from: data!)
                let code_str = industryList?.code
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        //print(industryList)
                        self.industryListData = industryList
                        //print(String(data: data!, encoding: String.Encoding.utf8))
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: "Error", message: "Error getting data", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }

    @IBAction func addButtonPressed(_ sender: Any) {
        var showAlert = 0
        if jobTitle.text == "" || currentSalary.text == "" || expectedSalary.text == "" || totalExperience.text == ""
        {
                showAlert = 1
        }
        for textField in pickerViewArray
        {
            if textField.text == ""
            {
                showAlert = 1
            }
            
        }
        if showAlert == 1
        {
            let alert = UIAlertController(title: "Update Profile", message: "Please fill in all fields", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
           
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            print("dad")
            makePostCall()

        }
    }
    func makePostCall() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let decoder = JSONDecoder()
        let json: [String: Any] = ["current_salary":"\(currentSalary.text!)","expected_max":"\(expectedSalary.text!)","expected_min":"\(expectedSalary.text!)","industry":"\(industry.text!)","job_category":"\(category.text!)","job_title":"\(jobTitle.text!)","jobseekerid":"\(id)","total_experience":"\(totalExperience.text!)","website":"\(socialLink.text ?? "")","code":0]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/update_professional")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            print("Success")
                            _ = self.navigationController?.popToRootViewController(animated: true)


                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Update Professional", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
}

extension UpdateProfileProfessional : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            if let safeData = industryListData
            {
                return safeData.industryList!.count
            }
            else
            {
                return 0
            }
            
        case 0:
            if let safeData = industryListData
            {
                if let x = safeData.industryList
                {
                    if let y = x[selectedIndustry].industrySub
                    {
                        return y.count
                    }
                    else
                    {
                        return 0
                    }
                }
                else
                {
                    return 0
                }
            }
            else
            {
                return 0
            }
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            if let safeData = industryListData
            {
                if let x = safeData.industryList
                {
                    return x[row].name
                }
                else
                {
                    return ""
                }
            }
            else
            {
                return ""
            }
        case 0:
            if let safeData = industryListData
            {
                if let x = safeData.industryList
                {
                    if let y = x[selectedIndustry].industrySub
                    {
                        return y[row].jobname!
                    }
                    else
                    {
                        return ""
                    }
                }
                else
                {
                    return ""
                }
            }
            else
            {
                return ""
            }
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 1 :
            selectedIndustry = row
            pickerViewArray[1].text = industryListData?.industryList?[row].name
        case 0:
            pickerViewArray[0].text = industryListData?.industryList?[selectedIndustry].industrySub?[row].jobname
        default :
            print(row)
}
    }
}
