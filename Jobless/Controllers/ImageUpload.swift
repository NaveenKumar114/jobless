//
//  ImageUpload.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-01.
//

import UIKit
import Alamofire
class ImageUpload: UIViewController {
    var profileImg : UIImage?
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile Upload"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contentView.layer.cornerRadius = 20
        let defaults = UserDefaults.standard

        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("https://jobless.my/uploads/jobseeker/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    if data != nil
                    {
                        print("profileImg load")
                        profileImage.image = UIImage(data: data!)
                        profileImage.contentMode = .scaleAspectFit
                        profileImage.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
                    }
                   
                }
            }
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toImage))
            profileImage.isUserInteractionEnabled = true
            profileImage.addGestureRecognizer(tapGestureRecognizer)


    }
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    
    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {
          
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                print("success")

                                if let str = loginBaseResponse?.logindetails?.profileImg
                                {
                                    UserDefaults.standard.setValue(str, forKey: "img")
                                    NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                                }
                                // NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
                                let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                                    _ = self.navigationController?.popViewController(animated: true)

                                   // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                                    print("Success")

                                } ))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Update Profile", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    
    
  


}

extension ImageUpload : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
        let id = UserDefaults.standard.string(forKey: "id")!
        let json: [String: Any] = ["jobseekerid" : "\(id)"]
        profileImg = image
        profileImage.image = image
        profileImage.contentMode = .scaleAspectFit
        profileImage.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        uploadImage(image: imageData!, to: URL(string: "https://jobless.my/andorid_api/Imagehelper/jobseeker_profileupload")!, params: json)

        
    }
}

extension URLEncoding {
    public func queryParameters(_ parameters: [String: Any]) -> [(String, String)] {
        var components: [(String, String)] = []
        
        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += queryComponents(fromKey: key, value: value)
        }
        return components
    }
}
