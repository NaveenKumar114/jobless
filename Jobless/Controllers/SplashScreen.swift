//
//  SplashScreen.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-07.
//

import UIKit
import FLAnimatedImage
class SplashScreen: UIViewController {
    @IBOutlet weak var centerLabel: UILabel!
    var timerJob : Timer?
    var timerCountJob = 0
    var timerBuild : Timer?
    var timerCountBuild = 0
    var timerCountCenter = 1
    var timerFind : Timer?
    var findCount = 0
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var findYour: UILabel!
    @IBOutlet weak var joblessBottom: UILabel!
    @IBOutlet weak var joblessLabel: UILabel!
    @IBOutlet weak var logoGif: FLAnimatedImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let resourcePath = Bundle.main.resourcePath {
            let imgName = "logo_jobless_gif.gif"
            let path = resourcePath + "/" + imgName
            let imageData1 = try? FLAnimatedImage(animatedGIFData: Data(contentsOf: URL(fileURLWithPath: path)))
            logoGif.animatedImage = imageData1
            
        }
        timerJob = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(fireTimerJob), userInfo: nil, repeats: true)
        timerBuild = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(centeTimer), userInfo: nil, repeats: true)
        timerFind = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(findTimer), userInfo: nil, repeats: true)
    }
    
    @objc func fireTimerJob() {
        let job = "www.jobless.my"
        if timerCountJob <= job.count
        {
            joblessBottom.text = String(job.prefix(timerCountJob))
            timerCountJob = timerCountJob+1
        }
        let build = "Let Us\"Build\" A Job For You"
        buildLabel.text = String(build.prefix(timerCountBuild))
        timerCountBuild = timerCountBuild+1
        if timerCountBuild > build.count
        {
            timerJob?.invalidate()
            }
    }
    @objc func centeTimer()
    {
        switch timerCountCenter {
        case 0:
            companyLabel.isHidden = true
            joblessLabel.isHidden = false
            timerCountCenter = 1
        case 1:
            companyLabel.isHidden = false
            joblessLabel.isHidden = true
            timerCountCenter = 0
        default:
            print("erroe")
        }
        
    }
    @objc func findTimer()
    {
        switch findCount {
        case 0:
            let attributedWithTextColor: NSAttributedString = "FIND YOUR DREAM".attributedStringWithColor(["DREAM"], color: UIColor.systemYellow)

            findYour.attributedText = attributedWithTextColor
        case 1:
            let attributedWithTextColor: NSAttributedString = "FIND YOUR CAREER".attributedStringWithColor(["CAREER"], color: UIColor.red)
            findYour.attributedText = attributedWithTextColor
        case 2:
            let attributedWithTextColor: NSAttributedString = "FIND YOUR LIFE".attributedStringWithColor(["LIFE"], color: UIColor.systemGreen)
            findYour.attributedText = attributedWithTextColor
        case 3:
            let attributedWithTextColor: NSAttributedString = "FIND YOUR JOB".attributedStringWithColor(["JOB"], color: UIColor.red)
            findYour.attributedText = attributedWithTextColor
        default:
            timerFind?.invalidate()
            timerJob?.invalidate()
            timerBuild?.invalidate()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            
            // if user is logged in before
            
            // instantiate the main tab bar controller and set it as root view controller
            // using the storyboard identifier we set earlier
            let loginController = storyboard.instantiateViewController(identifier: "login")
            let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

            let dashboard = storyboard2.instantiateViewController(identifier: "dashboard")
            let loggedUsername = UserDefaults.standard.string(forKey: "isLoggedIn")
                if loggedUsername == "true" {
                    // instantiate the main tab bar controller and set it as root view controller
                    // using the storyboard identifier we set earlier
                    
                    (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)                }
            else
                {
                    (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)                }
        }
        findCount = findCount + 1
       
    }
    
    
}

extension String {
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }

        guard let characterSpacing = characterSpacing else {return attributedString}

        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

        return attributedString
    }
}
