//
//  Skills.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-25.
//

import UIKit

class Skills: UIViewController {
    var editData : SkillList?
    @IBOutlet weak var skillLevel: UITextField!
    @IBOutlet weak var skillName: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var addButton: UIButton!
    var pickerArray = ["Advanced" , "Intermediate" , "Beginner"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Skills"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contentView.layer.cornerRadius = 20
        addButton.layer.cornerRadius = 25
        addButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        addButton.layer.borderWidth = 3
        skillLevel.delegate = self
        createPickerView(textField: skillLevel)
        dismissPickerView()
        if let safeData = editData
        {
            skillName.text = safeData.skillName!
            let per = Int(safeData.type!)
            switch per {
            case 100:
                skillLevel.text = "Advanced"
            case 50:
                skillLevel.text = "Intermediate"
            case 10:
                skillLevel.text = "Beginner"
            default:
                print(per)
            }
        }
        skillName.delegate = self
        skillName.tag = 1000
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func createPickerView(textField : UITextField) {
           let pickerView = UIPickerView()
           pickerView.delegate = self
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
        skillLevel.inputAccessoryView = toolBar
    }
    @objc func action() {
          view.endEditing(true)
    }
    @IBAction func addButtonPressed(_ sender: Any) {
        if skillLevel.text == "" || skillName.text == ""
        {
            let alert = UIAlertController(title: "Add Skills", message: "Please fill in all fields", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
           
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            if editData == nil{
                makePostCall()
            }
            else
            {
                makePostCallUpdate()
            }
            
        }
    }
    func makePostCall() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["jobseekerid":"\(id)","name":"\(name)","percentage":"\(skillLevel.text!)","skill_name":"\(skillName.text!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/insert_skill")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Skills", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            _ = self.navigationController?.popViewController(animated: true)

                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Skills", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    func makePostCallUpdate() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["jobseekerid":"\(id)","name":"\(name)","percentage":"\(skillLevel.text!)","skill_name":"\(skillName.text!)" , "skillid":"\(editData!.skillid!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/update_skill")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Skills", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                            _ = self.navigationController?.popViewController(animated: true)

                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Skills", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
}

extension Skills : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        skillLevel.text = pickerArray[row]
    }
}
