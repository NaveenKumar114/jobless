//
//  ViewController.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-23.
//

import UIKit
import FLAnimatedImage
class Login: UIViewController, UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var joblessLabel: UILabel!
    @IBOutlet weak var passwordView: ShadowView!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var logoGifView: FLAnimatedImageView!
    @IBOutlet weak var loginButtom: UIButton!
    @IBOutlet weak var contentView: UIView!
    var timer : Timer?
    var timerCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        password.delegate = self
        emailText.delegate = self
        password.isSecureTextEntry = true
        contentView.layer.cornerRadius = 10
        loginButtom.layer.cornerRadius = self.loginButtom.frame.height / 2
        loginButtom.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        loginButtom.layer.borderWidth = 3
        if let resourcePath = Bundle.main.resourcePath {
            let imgName = "logo_jobless_gif.gif"
            let path = resourcePath + "/" + imgName
            let imageData1 = try? FLAnimatedImage(animatedGIFData: Data(contentsOf: URL(fileURLWithPath: path)))
            logoGifView.animatedImage = imageData1
            
        }
         timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
       // timer?.fire()

    }
    
    @IBAction func forgotPasswordSwitch(_ sender: Any) {
        let s = sender as! UISwitch
        if s.isOn
        {
            password.isSecureTextEntry = true
        }
        else
        {
            password.isSecureTextEntry = false
        }
    }
    @objc func fireTimer() {
        let job = "www.jobless.my"
        joblessLabel.text = String(job.prefix(timerCount))
        timerCount = timerCount+1
        if timerCount > job.count
        {
            timer?.invalidate()
        }
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        if emailText.text?.isValidEmail == false
        {
            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if password.text == ""
        {
            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall(email: emailText.text!, password: password.text!)
        }
    }
    
    func makePostCall(email : String , password : String) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["email": "\(email)",
                                   "password": "\(password)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/jobseeker/login")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                let code_str = loginBaseResponse!.code
               

                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                 //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                       // print("success")
                        print(loginBaseResponse as Any)
                        let defaults = UserDefaults.standard
                        defaults.setValue("true", forKey: "isLoggedIn")
                        defaults.setValue((loginBaseResponse?.logindetails?.jobseekerid)!, forKey: "id")
                        defaults.setValue((loginBaseResponse?.logindetails?.firstName)!, forKey: "name")
                        if let str = loginBaseResponse?.logindetails?.profileImg
                        {
                            defaults.setValue(str, forKey: "img")

                        }
                        //defaults.setValue((loginBaseResponse?.logindetails?.lastName)!, forKey: "id")


                        let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                        let dashboard = storyboard2.instantiateViewController(identifier: "dashboard")
                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                        
                    }else if code_str == 201  {
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                        
                        let alert = UIAlertController(title: "Login", message: "\(loginBaseResponse?.response ?? "Invalid Email & Password")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }

}


