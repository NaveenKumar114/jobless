//
//  EducationDetails.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-25.
//

import UIKit

class EducationDetails: UIViewController {
    var editData : EducationList?
    @IBOutlet weak var collegeName: UITextField!
    @IBOutlet weak var year: UITextField!
    var departmentList = [DepartmentList]()
    var pickerViewArray = [UITextField]()
    var gradeList = ["Grade A" , "Grade B" , "Grade C" ,"Grade D" , "1st Class" , "2nd Class Upper" , "2nd Class Lower" , "3rd Class" , "CGPA/Percentage" , "Pass/Non-Gradable" , "Fail" , "Incomplete" , "On-going"]
    var qualificationList = ["Advanced Diploma" , "Bachelors" , "Certificate / Matriculation" , "Diploma" , "PHD / Doctorate"]
    @IBOutlet weak var grade: UITextField!
    @IBOutlet weak var fieldOfStudy: UITextField!
    @IBOutlet weak var qualification: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        makeGetCall()
        self.navigationItem.title = "Education Details"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        contentView.layer.cornerRadius = 20
        addButton.layer.cornerRadius = 25
        addButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        addButton.layer.borderWidth = 3
        pickerViewArray = [qualification , fieldOfStudy , grade]
        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)

        }
        dismissPickerView()
        if let safeData = editData
        {
            qualification.text = safeData.qualification!
            collegeName.text = safeData.university!
            year.text = safeData.passingYear!
            fieldOfStudy.text = safeData.fieldStudy!
            grade.text = safeData.grade!
        }
        collegeName.delegate = self
        year.delegate = self
        collegeName.tag = 1000
        year.tag = 1001
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func createPickerView(textField : UITextField , tag : Int) {
           let pickerView = UIPickerView()
           pickerView.delegate = self
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    @objc func action() {
          view.endEditing(true)
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
    
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://jobless.my/andorid_api/Jobseeker_helper/jobdepartment")! as URL)
        request.httpMethod = "GET"
        //      let postString = " Kidapi/getKids/44"
        //     print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //   request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let industryList = try? decoder.decode(DepartmentJSON.self, from: data!)
                let code_str = industryList?.code
                print(String(data: data!, encoding: String.Encoding.utf8))

                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        //print(industryList)
                        if let safeData = industryList?.departmentList
                        {
                            self.departmentList = safeData
                        }
                        //print(String(data: data!, encoding: String.Encoding.utf8))
                    }else if code_str == 201  {
                        
                        let alert = UIAlertController(title: "Error", message: "Error getting data", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            
            
        }
        task.resume()
    }
    @IBAction func addButtonPressed(_ sender: Any) {
        var showAlert = 0
        if collegeName.text == "" || year.text == ""
        {
            showAlert = 1
        }
        for textField in pickerViewArray
        {
            if textField.text == ""
            {
                showAlert = 1
            }
            
        }
        if showAlert == 1
        {
            let alert = UIAlertController(title: "Add Language", message: "Please fill in all fields", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
           
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }
        else
        {
            if editData == nil
            {
                makePostCall()
            }
            else
            {
                makePostCallUpdate()
            }
        }
    }
    
    func makePostCall() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["field_study":"\(fieldOfStudy.text!)","grade":"\(grade.text!)","jobseekerid":"\(id)","name":"\(name)","outof_cgpa":"0","passing_year":"\(year.text!)","qualification":"\(qualification.text!)","score_cgpa":"0","university":"\(collegeName.text!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/insert_education")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Education", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            _ = self.navigationController?.popViewController(animated: true)

                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Education", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    func makePostCallUpdate() {
        let id = UserDefaults.standard.string(forKey: "id")!
        let name = UserDefaults.standard.string(forKey: "name")!

        
        let decoder = JSONDecoder()
        let json: [String: Any] = ["field_study":"\(fieldOfStudy.text!)","grade":"\(grade.text!)","jobseekerid":"\(id)","name":"\(name)","outof_cgpa":"0","passing_year":"\(year.text!)","qualification":"\(qualification.text!)","score_cgpa":"0","university":"\(collegeName.text!)" ,"educationid":"\(editData!.educationid!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/insert_education")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        NotificationCenter.default.post(name: Notification.Name("update"), object: nil)

                        let alert = UIAlertController(title: "Add Education", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                            _ = self.navigationController?.popViewController(animated: true)

                           // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                            print("Success")

                        } ))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Add Education", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    
}
extension EducationDetails : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return qualificationList.count
        case 1:
            return departmentList.count
        case 2:
            return gradeList.count
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return qualificationList[row]
        case 1:
            return departmentList[row].studyDepartment!
        case 2:
            return gradeList[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
 
        switch pickerView.tag {
        case 0:
            pickerViewArray[0].text = qualificationList[row]
        case 1:
            if departmentList.count != 0
            {
            pickerViewArray[1].text = departmentList[row].studyDepartment!
            }
        case 2:
            pickerViewArray[2].text = gradeList[row]
        default:
            print("err")
        }
    
}
}
