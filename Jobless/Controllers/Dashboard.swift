//
//  Dashboard.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-26.
//

import UIKit
import SwipeCellKit
class Dashboard: UIViewController, SwipeCollectionViewCellDelegate {
    

    @IBOutlet weak var mainCollectionView: UICollectionView!
    var profileData : GetProfileJSON?
    var profileImage : UIImage?
    var percent = 0
    static var imageStatic : UIImage?
    static var profileDataStatic : GetProfileJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Home"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        mainCollectionView.register(UINib(nibName: "DashboardCollectionTop", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.dashboardTop)
        mainCollectionView.register(UINib(nibName: "WorkExperienceCollection", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.workExperience)
        mainCollectionView.register(UINib(nibName: "EducationCollection", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.education)
        mainCollectionView.register(UINib(nibName: "SkillsCollection", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.skills)
        mainCollectionView.register(UINib(nibName: "LanguageCollection", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.language)
        
        mainCollectionView.register(UINib(nibName: "LanguageHeading", bundle: nil), forCellWithReuseIdentifier: reuseIdentifiers.languageHeading)
        self.mainCollectionView.register(UINib(nibName: "DashboardCollectionSection", bundle: nil), forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: reuseIdentifiers.dashboardSection)
        mainCollectionView.isUserInteractionEnabled = true
        makePostCall()
        //let defaults = UserDefaults.standard
        NotificationCenter.default.addObserver(self, selector: #selector(updateData), name: Notification.Name("update"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toProf), name: Notification.Name("profile"), object: nil)
        updateData()
        
    }
    
    func calculatePercentage()
    {
        percent = 0
        if let safeData = profileData
        {
            let e = safeData.educationList?.count ?? 0
            let s = safeData.skillList?.count ?? 0
            let l = safeData.languageList?.count ?? 0
            let j = safeData.experinceList?.count ?? 0
            let d = [e , s , l , j]
            for n in d
            {
                switch n {
                case 0:
                    percent = percent + 0
                case 1:
                    percent = percent + 10
                default:
                    percent = percent + 20

                }
                print(percent)
            }
            if safeData.personaldetails != nil
            {
                percent = percent + 10
            }
            if safeData.personaldetails?.jobTitle != nil
            {
                percent = percent + 10

            }
        }
    }
    
    @objc func toProf()
    {
        performSegue(withIdentifier: segueIdentifiers.dashToProf, sender: nil)
    }
    @objc func updateData()
    {
        makePostCall()
        let defaults = UserDefaults.standard
        
        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("https://jobless.my/uploads/jobseeker/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    if data != nil
                    {
                        profileImage = UIImage(data: data!)
                        print("profileImg load")
                        mainCollectionView.reloadData()
                        Dashboard.imageStatic = profileImage
                    }
                    
                    //   logoImageVIew.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 20)
                }
            }
        }
        
    }
    @IBAction func logoutPressed(_ sender: Any) {
        let alert = UIAlertController(title: "LogOut", message: "Are you Sure", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey: "id")
            defaults.removeObject(forKey: "name")
            defaults.removeObject(forKey: "isLoggedIn")
            defaults.removeObject(forKey: "img")
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            
         
            let loginController = storyboard.instantiateViewController(identifier: "login")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    func makePostCall() {
        let decoder = JSONDecoder()
        let id = UserDefaults.standard.string(forKey: "id")
        let json: [String: Any] = ["jobseekerid": "\(id!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/Jobseeker_helper/get_complete_profile")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(GetProfileJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                        if let safeData = loginBaseResponse
                        {
                            self.profileData = safeData
                            self.calculatePercentage()

                            Dashboard.profileDataStatic = safeData
                            //print(Dashboard.profileDataStatic)
                            self.mainCollectionView.reloadData()
                        }
                        
                        // print("success")
                        // print(loginBaseResponse as Any)
                        
                        
                    }else   {
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Home", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @objc func toImage()
    {
        performSegue(withIdentifier: segueIdentifiers.dashToImage, sender: nil)
    }
}

extension Dashboard: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            var n = 0
            if let x = profileData?.experinceList?.count
            {
                n = x
            }
            return n
        case 2:
            var n = 0
            if let x = profileData?.educationList?.count
            {
                n = x
            }
            return n
        case 3:
            var n = 0
            if let x = profileData?.skillList?.count
            {
                n = x
            }
            return n
        case 4:
            var n = 0
            if let x = profileData?.languageList?.count
            {
                n = x + 1
            }
            return n
        default:
            return 0
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: nil) { [self] action, indexPath in

            let alert = UIAlertController(title: "Delete", message: "Are you sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                switch indexPath.section
                {
                case 1 :
                    if (profileData?.experinceList) != nil
                    {
                        let n = indexPath.row
                        let x = profileData?.experinceList?[n].experienceid
                        let url = "https://jobless.my/andorid_api/Jobseeker_helper/delete_experience/\(x!)"
                       makeGetCallDelete(titleForAlert: "Experience", url: url, indexPath: indexPath)
                    }
                case 2 :
                    if (profileData?.educationList) != nil
                    {
                        let n = indexPath.row
                        let x = profileData?.educationList?[n].educationid
                        let url = "https://jobless.my/andorid_api/Jobseeker_helper/delete_education/\(x!)"
                       makeGetCallDelete(titleForAlert: "Education", url: url, indexPath: indexPath)
                    }
                case 3 :
                    
                    if (profileData?.skillList) != nil
                    {
                        let n = indexPath.row
                        let x = profileData?.skillList?[n].skillid
                        let url = "https://jobless.my/andorid_api/Jobseeker_helper/delete_skill/\(x!)"
                       makeGetCallDelete(titleForAlert: "Skills", url: url, indexPath: indexPath)
                    }
                case 4 :
                    if (profileData?.languageList) != nil
                    {
                        let n = indexPath.row - 1
                        let x = profileData?.languageList?[n].languageid
                        let url = "https://jobless.my/andorid_api/Jobseeker_helper/delete_language/\(x!)"
                       makeGetCallDelete(titleForAlert: "Language", url: url, indexPath: indexPath)
                    }

                default :
                    print("sss")
                }
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })

           
        }
        let editAction = SwipeAction(style: .default, title: nil) { [self] action, indexPath in
            // handle action by updating model with deletion
            print("edit")
            //print(indexPath.row , indexPath.section)
            switch indexPath.section
            {
            case 1 :
                if let safeData = profileData?.experinceList
                {
                    let n = indexPath.row
                    //print(safeData[n])
                    let data = [1 , safeData[n]] as [Any]
                    performSegue(withIdentifier: segueIdentifiers.dashToWork, sender: data)
                }
            case 2 :
                if let safeData = profileData?.educationList
                {
                    let n = indexPath.row
                    //print(safeData[n])
                    let data = [2 , safeData[n]] as [Any]
                    performSegue(withIdentifier: segueIdentifiers.dashToEdu, sender: data)
                }
            case 3 :
                
                if let safeData = profileData?.skillList
                {
                    let n = indexPath.row
                    //print(safeData[n])
                    let data = [3 , safeData[n]] as [Any]
                    performSegue(withIdentifier: segueIdentifiers.dashToSkills, sender: data)
                }
            case 4 :
                //education
                if let safeData = profileData?.languageList
                {
                    let n = indexPath.row - 1  //Beacuse of the written spoken row
                    //print(safeData[n])
                    let data = [4 , safeData[n]] as [Any]
                    performSegue(withIdentifier: segueIdentifiers.dashToLang, sender: data)
                }

            default :
                print("sss")
            }
        }
        // customize the action appearance
        deleteAction.image = UIImage(systemName: "trash")
        editAction.image =  UIImage(systemName: "pencil")
        
        return [deleteAction , editAction]
    }
    
    
    func makeGetCallDelete(titleForAlert : String , url : String , indexPath : IndexPath) {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
       let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                let responseJson = try? decoder.decode(DeleteResponse.self, from: data!)
                let code_str = responseJson?.code
                DispatchQueue.main.async {
                    if code_str == 200 {
                        self.mainCollectionView.deleteItems(at: [indexPath])
                        switch indexPath.section
                        {
                        case 1 :
                            print(self.profileData?.experinceList?.remove(at: indexPath.row) as Any)

                        case 2 :
                            print(self.profileData?.educationList?.remove(at: indexPath.row) as Any)

                        case 3 :
                            
                            print(self.profileData?.skillList?.remove(at: indexPath.row) as Any)

                        case 4 :
                                print(self.profileData?.languageList?.remove(at: indexPath.row - 1) as Any)


                        default :
                            print("sss")
                        }
                        let alert = UIAlertController(title: titleForAlert , message: responseJson?.response ?? "deleted" , preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)                    }
                    else if code_str == 201  {
                        let alert = UIAlertController(title: titleForAlert, message: "Error", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let s = sender as? [Any]
        {
            if s[0] as! Int == 1
            {
                let destinationVC = segue.destination as! WorkExperience
                destinationVC.editData = s[1] as? ExperinceList
            }
            if s[0] as! Int == 2
            {
                let destinationVC = segue.destination as! EducationDetails
                destinationVC.editData = s[1] as? EducationList
            }
            if s[0] as! Int == 3
            {
                let destinationVC = segue.destination as! Skills
                destinationVC.editData = s[1] as? SkillList
            }
            if s[0] as! Int == 4
            {
                let destinationVC = segue.destination as! Language
                destinationVC.editData = s[1] as? LanguageList
            }

        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            
            let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.dashboardTop, for: indexPath) as! DashboardCollectionTop
            
            if let safeData = profileData
            {
                cell.progressBar.value = CGFloat(percent)
                cell.nameLabel.text = "\(safeData.personaldetails?.firstName ?? "") \(safeData.personaldetails?.lastName ?? "")"
                cell.emailLabel.text = "\(safeData.personaldetails?.email ?? "")"
                cell.moneyLabel.text = "\(safeData.personaldetails?.expectedMin ?? "")MYR - \(safeData.personaldetails?.expectedMax ?? "")MYR"
                cell.locationLabel.text = safeData.personaldetails?.address ?? "."
                cell.phoneLabel.text = safeData.personaldetails?.mobile ?? "."
                cell.yearLabel.text = "\(safeData.personaldetails?.totalExperience ?? "") Year"
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                
                let modifyDate = dateFormatter.date(from: (safeData.personaldetails?.modifyDate)!)
                let date = NSDate()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                formatter.timeZone = TimeZone(abbreviation: "UTC")
                let defaultTimeZoneStr = formatter.string(from: date as Date)
                let currentDate = dateFormatter.date(from: defaultTimeZoneStr)
                let interval = currentDate!.timeIntervalSince(modifyDate!)
                cell.lastUpdatedLabel.text = "Last Updated \(interval.stringFromTimeInterval()) "
                cell.currentJob.text = safeData.personaldetails?.jobTitle ?? ""
                if profileImage != nil
                {
                    cell.profileImage.image = profileImage!
                    cell.profileImage.contentMode = .scaleAspectFit
                    cell.profileImage.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
                }
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toImage))
                cell.profileImage.isUserInteractionEnabled = true
                cell.profileImage.addGestureRecognizer(tapGestureRecognizer)
            }
            cell.isHidden = false // dont delete this
            
            return cell
            
        case 1:
            
            let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.workExperience, for: indexPath) as! WorkExperienceCollection
            cell.delegate = self
            
            if let safeData = profileData?.experinceList
            {
                let n = indexPath.row
                cell.companyLabel.text = safeData[n].companyName
                cell.titleLabel.text = safeData[n].jobTitle
                cell.yearLabel.text = "\(safeData[n].frommonth!) - \(safeData[n].fromyear!) Till \(safeData[n].tomonth!) - \(safeData[n].toyear!)"
                
            }
            cell.isHidden = false // dont delete this
            
            return cell
            
        case 2:
            
            let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.education, for: indexPath) as! EducationCollection
            cell.delegate = self
            
            if let safeData = profileData?.educationList
            {
                let n = indexPath.row
                cell.collegeLabel.text = safeData[n].university!
                cell.subjectLabel.text = safeData[n].fieldStudy!
                cell.yearLabel.text = safeData[n].passingYear!
            }
            cell.isHidden = false // dont delete this
            
            return cell
            
        case 3:
            
            let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.skills, for: indexPath) as! SkillsCollection
            //cell.isUserInteractionEnabled = true
            cell.delegate = self
            if let safeData = profileData?.skillList
            {
                let n = indexPath.row
                cell.skillLabel.text = safeData[n].skillName!
                cell.progressBar.progress = (Float(safeData[n].type!) ?? 0.0) / 100
            }
            cell.isHidden = false // dont delete this
            
            return cell
            
        case 4:
            if indexPath.row != 0
            {
                let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.language, for: indexPath) as! LanguageCollection
                cell.delegate = self
                
                if let safeData = profileData?.languageList
                {
                    let n = indexPath.row - 1
                    cell.languageLabel.text = safeData[n].languageName!
                    cell.spokenLabel.text = safeData[n].spoken!
                    cell.writtenLabel.text = safeData[n].written!
                }
                cell.isHidden = false // dont delete this
                
                return cell
            }
            else
            {
                let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.languageHeading, for: indexPath)
                return cell
                
            }
        default:
            let cell = mainCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifiers.dashboardTop, for: indexPath) as! DashboardCollectionTop
            cell.isHidden = true
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: mainCollectionView.frame.width, height: 180)
            
        case 1:
            return CGSize(width: mainCollectionView.frame.width, height: 80)
        case 2:
            return CGSize(width: mainCollectionView.frame.width, height: 60)
        case 3:
            return CGSize(width: mainCollectionView.frame.width, height: 30)
        case 4:
            if indexPath.row == 0
            {
                return CGSize(width: mainCollectionView.frame.width, height: 20)
            }
            else
            {
                return CGSize(width: mainCollectionView.frame.width, height: 35)
            }
        default:
            return CGSize(width: 0, height: 0)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseIdentifiers.dashboardSection, for: indexPath) as! DashboardCollectionSection
        switch indexPath.section {
        case 1:
            headerCell.titleLabel.text = "Work Experience:"
            headerCell.addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
            headerCell.addButton.tag = indexPath.section
        case 2:
            headerCell.titleLabel.text = "Education:"
            headerCell.addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
            headerCell.addButton.tag = indexPath.section
            
        case 3:
            headerCell.titleLabel.text = "Skills:"
            headerCell.addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
            headerCell.addButton.tag = indexPath.section
            
        case 4:
            headerCell.titleLabel.text = "Languages:"
            headerCell.addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
            headerCell.addButton.tag = indexPath.section
            
        default:
            print("err")
        }
        
        return headerCell
    }
    @objc func addButtonPressed(sender : UIButton)
    {
        
        switch sender.tag {
        case 1:
            performSegue(withIdentifier: segueIdentifiers.dashToWork, sender: self)
        case 2:
            performSegue(withIdentifier: segueIdentifiers.dashToEdu, sender: self)
        case 3:
            performSegue(withIdentifier: segueIdentifiers.dashToSkills, sender: self)
        case 4:
            performSegue(withIdentifier: segueIdentifiers.dashToLang, sender: self)
        default:
            print(sender.tag)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0
        {
            return CGSize(width:collectionView.frame.size.width, height:0)
            
        }
        else
        {
            return CGSize(width:collectionView.frame.size.width, height:70)
            
        }
    }
    
}

extension TimeInterval{
    
    func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        
        // let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        //let seconds = time % 60
        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        
        return String(format: "%0.2d Hr %0.2d Min",hours,minutes)
        
    }
}
