//
//  SignUp.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-24.
//

import UIKit
import FLAnimatedImage
class SignUp: UIViewController, UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var logo: FLAnimatedImageView!
    @IBOutlet weak var joblessLabel: UILabel!
    var timer : Timer?
    var timerCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        password.delegate = self
        confirmPassword.delegate = self
        email.delegate = self
        lastName.delegate = self
        firstName.delegate = self
        contentView.layer.cornerRadius = 10
        signUpButton.layer.cornerRadius = self.signUpButton.frame.height / 2
        signUpButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        signUpButton.layer.borderWidth = 3
        password.isSecureTextEntry = true
        confirmPassword.isSecureTextEntry = true
        if let resourcePath = Bundle.main.resourcePath {
            let imgName = "logo_jobless_gif.gif"
            let path = resourcePath + "/" + imgName
            let imageData1 = try? FLAnimatedImage(animatedGIFData: Data(contentsOf: URL(fileURLWithPath: path)))
            logo.animatedImage = imageData1
            
        }
         timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
       // timer?.fire()

    }
    @objc func fireTimer() {
        let job = "www.jobless.my"
        print(String(job.prefix(timerCount)))
        joblessLabel.text = String(job.prefix(timerCount))
        timerCount = timerCount+1
        if timerCount > job.count
        {
            timer?.invalidate()
        }
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func signUpPressed(_ sender: Any) {
        if firstName.text == "" || lastName.text == ""
        {
            let alert = UIAlertController(title: "Signup", message: "Invalid Name", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if email.text?.isValidEmail == false {
            let alert = UIAlertController(title: "Signup", message: "Invalid email", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
        else
        if password.text?.count ?? 0 < 4 {
            let alert = UIAlertController(title: "Signup", message: "Minimum password length 4", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
        else
        if password.text != confirmPassword.text
        {
            let alert = UIAlertController(title: "Signup", message: "Please confirm your password", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
         print("success")
            makePostCall(email: email.text!, password: password.text!, fName: firstName.text!, lName: lastName.text!)
        }
    }
    func makePostCall(email: String , password: String , fName: String , lName : String) {
        let decoder = JSONDecoder()
     
        let json: [String: Any] = ["email": "\(email)",
                                   "password": "\(password)",
                                   "first_name": "\(fName)",
                                   "last_name": "\(lName)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "https://jobless.my/andorid_api/RegisterApi/jobseekerRegister")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(RegisterJSON.self, from: data!)
                
                
                let code_str = loginBaseResponse?.code
                print(String(data: data!, encoding: String.Encoding.utf8))

                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Registered successfully.Kindly check your email to verify your account")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        print("success")
                        print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        
                        
                        let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
            //                catch {
            //                    self.removeSpinner()
            //                    print("Error -> \(error)")
            //                    showAlert(title: "Server Error", message: "Try Again later")
            //                }
        }
        task.resume()
    }
}

extension String {
    var isValidEmail: Bool {
        let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
        return testEmail.evaluate(with: self)
    }
}
