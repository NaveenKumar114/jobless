//
//  GetProfileJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-26.
//

import Foundation

struct GetProfileJSON: Codable {
    let code: Int?
    let response: String?
    let personaldetails: Personaldetails?
    var experinceList: [ExperinceList]?
    var educationList: [EducationList]?
    var skillList: [SkillList]?
    var languageList: [LanguageList]?

    enum CodingKeys: String, CodingKey {
        case code, response, personaldetails
        case experinceList = "experince_list"
        case educationList = "education_list"
        case skillList = "skill_list"
        case languageList = "language_list"
    }
}

// MARK: - EducationList
struct EducationList: Codable {
    let educationid, jobseekerid, qualification, university: String?
    let passingYear, grade, fieldStudy, scoreCgpa: String?
    let outofCgpa, totalPercentage, percentage, createBy: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case educationid, jobseekerid, qualification, university
        case passingYear = "passing_year"
        case grade
        case fieldStudy = "field_study"
        case scoreCgpa = "score_cgpa"
        case outofCgpa = "outof_cgpa"
        case totalPercentage = "total_percentage"
        case percentage
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - ExperinceList
struct ExperinceList: Codable {
    let experienceid, jobseekerid, jobTitle, companyName: String?
    let fromdate, todate, fromyear, frommonth: String?
    let toyear, tomonth, location, createBy: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case experienceid, jobseekerid
        case jobTitle = "job_title"
        case companyName = "company_name"
        case fromdate, todate, fromyear, frommonth, toyear, tomonth, location
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - LanguageList
struct LanguageList: Codable {
    let languageid, jobseekerid, languageName, written: String?
    let spoken, primaryLevel, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case languageid, jobseekerid
        case languageName = "language_name"
        case written, spoken
        case primaryLevel = "primary_level"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - Personaldetails
struct Personaldetails: Codable {
    let jobseekerid, firstName, lastName, address: String?
    let pincode, city, email, mobile: String?
    let gender, birthdate, race, religion: String?
    let maritalStatus, raceOther, religionOther, maritalOther: String?
    let jobDesignation, jobDesignationID, jobTitle, currentSalary: String?
    let totalExperience, expectedMin, expectedMax, website: String?
    let country, state, nationality, otherNationality: String?
    let highestQualification, specialization, industryID, industry: String?
    let resume: String?
    let status, profileImg: String?
    let coverLetter, plan, planExpiry: String?
    let resumeStatus, password, viewStatusEmployer, createBy: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case jobseekerid
        case firstName = "first_name"
        case lastName = "last_name"
        case address, pincode, city, email, mobile, gender, birthdate, race, religion
        case maritalStatus = "marital_status"
        case raceOther = "race_other"
        case religionOther = "religion_other"
        case maritalOther = "marital_other"
        case jobDesignation = "job_designation"
        case jobDesignationID = "job_designation_id"
        case jobTitle = "job_title"
        case currentSalary = "current_salary"
        case totalExperience = "total_experience"
        case expectedMin = "expected_min"
        case expectedMax = "expected_max"
        case website, country, state, nationality
        case otherNationality = "other_nationality"
        case highestQualification = "highest_qualification"
        case specialization
        case industryID = "industry_id"
        case industry, resume, status
        case profileImg = "profile_img"
        case coverLetter = "cover_letter"
        case plan
        case planExpiry = "plan_expiry"
        case resumeStatus = "resume_status"
        case password
        case viewStatusEmployer = "view_Status_employer"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - SkillList
struct SkillList: Codable {
    let skillid, jobseekerid, skillName, type: String?
    let createBy, createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case skillid, jobseekerid
        case skillName = "skill_name"
        case type
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
