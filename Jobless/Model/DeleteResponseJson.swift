//
//  DeleteResponseJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-05.
//

import Foundation

// MARK: - DeleteResponse
struct DeleteResponse: Codable {
    let code: Int?
    let response: String?
}
