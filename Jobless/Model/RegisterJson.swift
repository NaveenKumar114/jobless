//
//  RegisterJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-25.
//

import Foundation

// MARK: - RegisterJSON
struct RegisterJSON: Codable {
    let code: Int?
    let response: String?
}
