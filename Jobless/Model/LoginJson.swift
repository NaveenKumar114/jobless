//
//  LoginJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-24.
//

import Foundation

// MARK: - LoginJSON
struct LoginJSON: Codable {
    let code: Int?
    let response: String?
    let logindetails: Logindetails?
}

// MARK: - Logindetails
struct Logindetails: Codable {
    let jobseekerid, firstName, lastName, address: String?
    let pincode, city, email, mobile: String?
    let gender, birthdate, race, religion: String?
    let maritalStatus, raceOther, religionOther, maritalOther: String?
    let jobDesignation, jobDesignationID, jobTitle, currentSalary: String?
    let totalExperience, expectedMin, expectedMax, website: String?
    let country, state, nationality, otherNationality: String?
    let highestQualification, specialization, industryID, industry: String?
    let resume: String?
    let status, profileImg: String?
    let coverLetter, plan, planExpiry: String?
    let resumeStatus, password, viewStatusEmployer, createBy: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case jobseekerid
        case firstName = "first_name"
        case lastName = "last_name"
        case address, pincode, city, email, mobile, gender, birthdate, race, religion
        case maritalStatus = "marital_status"
        case raceOther = "race_other"
        case religionOther = "religion_other"
        case maritalOther = "marital_other"
        case jobDesignation = "job_designation"
        case jobDesignationID = "job_designation_id"
        case jobTitle = "job_title"
        case currentSalary = "current_salary"
        case totalExperience = "total_experience"
        case expectedMin = "expected_min"
        case expectedMax = "expected_max"
        case website, country, state, nationality
        case otherNationality = "other_nationality"
        case highestQualification = "highest_qualification"
        case specialization
        case industryID = "industry_id"
        case industry, resume, status
        case profileImg = "profile_img"
        case coverLetter = "cover_letter"
        case plan
        case planExpiry = "plan_expiry"
        case resumeStatus = "resume_status"
        case password
        case viewStatusEmployer = "view_Status_employer"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
