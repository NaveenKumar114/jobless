//
//  ReuseIdentifiers.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-27.
//

import Foundation

struct reuseIdentifiers {
    static let workExperience = "workExperience"
    static let language = "languageCollection"
    static let skills = "skillsCollection"
    static let education = "educationExperience"
    static let dashboardTop = "dashboardTop"
    static let dashboardSection = "dashboardSection"
    static let languageHeading = "languageHeading"

}
struct segueIdentifiers{
    static let dashToLang = "toLanguage"
    static let dashToWork = "toWork"
    static let dashToEdu = "toEducation"
    static let dashToSkills = "toSkill"
    static let profToPersonal = "professionalToPersonal"
    static let profToImage = "updateToImage"
    static let dashToImage = "dashToImage"
    static let dashToProf = "dashToProf"
}
