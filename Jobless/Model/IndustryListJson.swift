//
//  IndustryListJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-01.
//

import Foundation
struct IndustryListJSON: Codable {
    let code: Int?
    let response: String?
    let industryList: [IndustryList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case industryList = "industry_list"
    }
}

// MARK: - IndustryList
struct IndustryList: Codable {
    let industryID, name: String?
    let industrySub: [IndustrySub]?

    enum CodingKeys: String, CodingKey {
        case industryID = "industry_id"
        case name
        case industrySub = "industry_sub"
    }
}

// MARK: - IndustrySub
struct IndustrySub: Codable {
    let id, jobname, industryID: String?

    enum CodingKeys: String, CodingKey {
        case id, jobname
        case industryID = "industry_id"
    }
}
