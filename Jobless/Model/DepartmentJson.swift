//
//  DepartmentJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-02.
//

import Foundation
struct DepartmentJSON: Codable {
    let code: Int?
    let response: String?
    let departmentList: [DepartmentList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case departmentList = "department_list"
    }
}

// MARK: - DepartmentList
struct DepartmentList: Codable {
    let id, studyDepartment, createBy, createID: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case studyDepartment = "study_department"
        case createBy = "create_by"
        case createID = "create_id"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
