//
//  LanguageJson.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-12-02.
//

import Foundation

// MARK: - LanguageJSON
struct LanguageJSON: Codable {
    let code: Int?
    let response: String?
    let languages: [Languages]?
}

// MARK: - Language
struct Languages: Codable {
    let id, language: String?
}
