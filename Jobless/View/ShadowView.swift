//
//  shadowview.swift
//  KidsMana
//
//  Created by Naveen Natrajan on 2020-09-24.
//  Copyright © 2020 Naveen Natrajan. All rights reserved.
//
import Foundation
import UIKit
class ShadowView: UIView {

    var setupShadowDone: Bool = false
    
    public func setupShadow() {
        if setupShadowDone { return }
        //print("Setup shadow!")
    //    self.layer.cornerRadius = 25
        self.layer.cornerRadius = self.frame.height / 2

        
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,
byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 25, height:
8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    
        setupShadowDone = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        setupShadow()
    }
}
