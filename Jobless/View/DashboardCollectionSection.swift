//
//  DashboardCollectionSection.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-26.
//

import UIKit

class DashboardCollectionSection: UICollectionViewCell {
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        addButton.layer.cornerRadius = 22
        addButton.layer.borderColor = #colorLiteral(red: 0.5336629152, green: 0.7095460296, blue: 0.3863976598, alpha: 1)
        addButton.layer.borderWidth = 3    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        print(titleLabel.text)
    }
}
