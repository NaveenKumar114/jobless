//
//  EducationCollection.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-27.
//

import UIKit
import SwipeCellKit
class EducationCollection: SwipeCollectionViewCell {
    @IBOutlet weak var collegeLabel: UILabel!
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var subjectLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
