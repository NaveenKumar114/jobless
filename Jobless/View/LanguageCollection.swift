//
//  LanguageCollection.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-27.
//

import UIKit
import SwipeCellKit
class LanguageCollection: SwipeCollectionViewCell {

    @IBOutlet weak var writtenLabel: UILabel!
    @IBOutlet weak var spokenLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
