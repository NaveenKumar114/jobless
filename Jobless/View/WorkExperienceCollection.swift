//
//  WorkExperienceCollection.swift
//  Jobless
//
//  Created by Naveen Natrajan on 2020-11-27.
//

import UIKit
import SwipeCellKit
class WorkExperienceCollection: SwipeCollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
